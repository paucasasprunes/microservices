package com.greyfinch.financial.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/rest", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ReceivablesController {

    @RequestMapping(path = "/receivables", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public String findAll() {
        return "list of receivables";
    }

}
